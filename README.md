# About
This is an example implementation of a 'Comment' microservice written in Kotlin using Ktor. You could consider this a proof of concept at best as there's much lacking to call it complete "production ready" code. 

# Configuration

* [Application.kt](src/main/kotlin/org/phillipz/Application.kt) is the 'main' starting point where all modules are configured.
* For Server, JWT, and Test configuration, see [application.conf](src/main/resources/application.conf)

# Running

* Run the `main()` fn in [Application.kt](src/main/kotlin/org/phillipz/Application.kt)
* If `testMode` is enabled, a test JWT token will be output to the console. _(You'll need this to perform calls to the Service)_
* [TestData](src/main/kotlin/org/phillipz/kommentor/repo/CommentData.kt) will be loaded at startup
 
Some examples running using [httpie](https://httpie.io/):

```shell
# Save your token in an envvar
> export TOKEN="Bearer {YOUR_COPIED_TOKEN_HERE}"

# List comment for a given 'Subject Id'
> http -v :8080/comments subjectId==fafd182a-939b-4495-b5ce-4f763ec1db2a Authorization:$TOKEN

# Create a new comment
> http -v :8080/comments subjectId=fafd182a-939b-4495-b5ce-4f763ec1db2a text="My new comment" Authorization:$TOKEN

# Edit that comment (You'll need to utilize the 'id' from the previous POST)
> http -v PUT :8080/comments/{YOUR_COPIED_ID_HERE} text=="Updated text!!!" Authorization:$TOKEN

# Delete that comment
> http -v DELETE :8080/comments/{YOUR_COPIED_ID_HERE} Authorization:$TOKEN
```

# Limitations/Restrictions

The following are things that were not implemented for this simple PoC but would likely be considerations for a more production ready version of this service

* No pagination
* No explicit sorting or filtering controls in the API
* No Authorization (e.g. that a user has permission to view/edit a comment for a given subject)
* No Push (Websockets, XMPP, etc.)
* No History (only maintaining current state of comments)


_**Note**: All the files found under [org.phillipz.kommentor.config](src/main/kotlin/org/phillipz/kommentor/config) are more general purpose files and would ideally be moved to a shared library for utilizing across other micro-services._

