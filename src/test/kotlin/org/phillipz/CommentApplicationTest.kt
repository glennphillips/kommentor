package org.phillipz

import com.fasterxml.jackson.annotation.JsonInclude
import com.fasterxml.jackson.databind.DeserializationFeature
import com.fasterxml.jackson.databind.ObjectMapper
import com.fasterxml.jackson.module.kotlin.readValue
import com.fasterxml.jackson.module.kotlin.registerKotlinModule
import com.typesafe.config.ConfigFactory
import io.ktor.config.*
import io.ktor.http.*
import io.ktor.server.testing.*
import org.phillipz.kommentor.config.generateToken
import org.phillipz.kommentor.config.jwtConfig
import org.phillipz.kommentor.config.testUser
import kotlin.test.Test
import kotlin.test.assertEquals
import kotlin.test.assertFailsWith
import kotlin.test.assertNotNull
import kotlin.test.assertNull

class CommentApplicationTest {
    private val config = HoconApplicationConfig(ConfigFactory.load())
    private val token = generateToken(config.jwtConfig(), config.testUser())
    private val objectMapper = ObjectMapper()
        .registerKotlinModule()
        .configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false)
        .setSerializationInclusion(JsonInclude.Include.NON_NULL)
        .setDefaultPropertyInclusion(JsonInclude.Value.construct(JsonInclude.Include.ALWAYS, JsonInclude.Include.NON_NULL))

    @Test
    fun testGetCommentsSuccess() {
        withTestApplication({ configureModules(config) }) {
            with(handleRequest(HttpMethod.Get, "/comments?subjectId=368578be-6319-4127-823e-0d4b9813a3c8") {
                withAuth()
            }) {
                assertEquals(HttpStatusCode.OK, response.status())
                // TODO: serialize to java object and assert
                assertNotNull(response.content)
            }
        }
    }

    @Test
    fun testGetCommentsMissingSubjectId() {
        withTestApplication({ configureModules(config) }) {
            val exception = assertFailsWith<IllegalArgumentException> {
                handleRequest(HttpMethod.Get, "/comments") { withAuth() }
            }
            assertEquals("A 'subjectId' query parameter is required to retrieve comments", exception.message)
        }
    }

    @Test
    fun testGetCommentsNoAuth() {
        withTestApplication({ configureModules(config) }) {
            handleRequest(HttpMethod.Get, "/comments").apply {
                assertEquals(HttpStatusCode.Unauthorized, response.status())
                assertNull(response.content)
            }
        }
    }

    @Test
    fun testCreateCommentSuccess() {
        withTestApplication({ configureModules(config) }) {
            handleRequest(HttpMethod.Post, "/comments") {
                withAuth()
                withJson()
                setBody("""
                   {
                        "subjectId": "368578be-6319-4127-823e-0d4b9813a3c8",
                        "text": "Test Comment 1"
                   }
                """.trimIndent())
            }.apply {
                assertEquals(HttpStatusCode.OK, response.status())
                objectMapper.readValue<Map<String, Any>>(response.content ?: "").let {
                    assertEquals(it["text"], "Test Comment 1")

                    // Fetch the same by id and verify equal
                    handleRequest(HttpMethod.Get, "/comments/${it["id"]}") { withAuth() }.apply {
                        assertEquals(HttpStatusCode.OK, response.status())
                        assertEquals(it, objectMapper.readValue(response.content ?: ""))
                    }
                }
            }
        }
    }

    private fun TestApplicationRequest.withAuth() = addHeader(HttpHeaders.Authorization, "Bearer $token")
    private fun TestApplicationRequest.withJson() = addHeader(HttpHeaders.ContentType, ContentType.Application.Json.toString())

}