package org.phillipz.kommentor.config

import com.auth0.jwt.JWT
import com.auth0.jwt.algorithms.Algorithm
import io.ktor.application.*
import io.ktor.auth.*
import io.ktor.auth.jwt.*
import io.ktor.config.*
import java.time.Instant
import java.util.*

/**
 * The Principal construct used by Ktor for authentication/authorization purposes
 */
data class User(val userName: String, val firstName: String, val lastName: String) : Principal

/**
 * Enables JWT authentication in Ktor
 */
fun Application.configureSecurity(config: ApplicationConfig) {
    with(config.jwtConfig()) {
        authentication {
            jwt {
                this@jwt.realm = this@with.realm
                verifier(
                    JWT.require(Algorithm.HMAC256(secret))
                        .withAudience(audience)
                        .withIssuer(issuer)
                        .build()
                )
                validate { credential ->
                    if (credential.hasAudience(audience)) User(
                        userName = credential.getClaim("userName"),
                        firstName = credential.getClaim("firstName"),
                        lastName = credential.getClaim("lastName")
                    )
                    else null
                }
            }
        }

        if (config.isTestMode()) {
            println("===========================================================================")
            println("You can test this API with this token (valid for ${expiration / 60} minutes):")
            println(generateToken(this, config.testUser()))
            println("===========================================================================")
        }
    }
}

private fun JWTCredential.hasAudience(audience: String): Boolean = payload.audience.contains(audience)
private fun JWTCredential.getClaim(claimName: String, default: String = ""): String =
    payload.getClaim(claimName)?.asString() ?: default

/**
 * Under normal circumstances, this fn would be called as part of a successful authentication. However, for our
 * purposes, it's simply used to generate a test token at startup (when in testMode)
 */
fun generateToken(jwtConfig: JWTConfig, user: User): String = with(jwtConfig) {
    return JWT.create()
        .withAudience(audience)
        .withIssuer(issuer)
        .withClaim("userName", user.userName)
        .withClaim("firstName", user.firstName)
        .withClaim("lastName", user.lastName)
        .withExpiresAt(Date.from(Instant.now().plusSeconds(expiration)))
        .sign(Algorithm.HMAC256(secret))
}

/**
 * extension solely used to generate our 'Test' user when in testMode
 */
fun ApplicationConfig.testUser() = User(
    this.stringProperty("test.user.id"),
    this.stringProperty("test.user.firstName"),
    this.stringProperty("test.user.lastName")
)




