package org.phillipz.kommentor.config

import io.ktor.config.*

data class JWTConfig(
    val audience: String,
    val realm: String,
    val issuer: String,
    val secret: String,
    val expiration: Long
)

fun ApplicationConfig.jwtConfig() = with(this.config("jwt")) {
    JWTConfig(
        audience = stringProperty("audience"),
        realm = stringProperty("realm"),
        issuer = stringProperty("issuer"),
        secret = stringProperty("secret"),
        expiration = longProperty("expiration")
    )
}
