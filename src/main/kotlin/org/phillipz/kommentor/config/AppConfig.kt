package org.phillipz.kommentor.config

import io.ktor.config.*

fun ApplicationConfig.stringProperty(propName: String): String = this.property(propName).getString()
fun ApplicationConfig.stringPropertyOrNull(propName: String): String? = this.propertyOrNull(propName)?.getString()

fun ApplicationConfig.booleanProperty(propName: String): Boolean = stringProperty(propName).toBoolean()
fun ApplicationConfig.booleanPropertyOrNull(propName: String): Boolean? = stringPropertyOrNull(propName)?.toBoolean()

fun ApplicationConfig.intProperty(propName: String): Int = stringProperty(propName).toInt()
fun ApplicationConfig.intPropertyOrNull(propName: String): Int? = stringPropertyOrNull(propName)?.toIntOrNull()

fun ApplicationConfig.longProperty(propName: String): Long = stringProperty(propName).toLong()
fun ApplicationConfig.longPropertyOrNull(propName: String): Long? = stringPropertyOrNull(propName)?.toLongOrNull()

// Test mode
fun ApplicationConfig.isTestMode(): Boolean = booleanPropertyOrNull("test.enabled") ?: false


