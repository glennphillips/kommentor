package org.phillipz.kommentor.config

import io.ktor.application.*
import io.ktor.config.*
import io.ktor.features.*
import io.ktor.features.StatusPages.*
import io.ktor.http.*
import io.ktor.response.*
import org.phillipz.kommentor.model.APIError

fun Application.configureExceptionHandling(config: ApplicationConfig) {
    install(StatusPages) {
        // Catch All
        handleError<Throwable>(HttpStatusCode.InternalServerError)
        // Bad input
        handleError<IllegalArgumentException>(HttpStatusCode.BadRequest)
        // Not found
        handleError<NotFoundException>(HttpStatusCode.NotFound)
    }
}

/**
 * Standard API Error handling...
 */
inline fun <reified T : Throwable> Configuration.handleError(httpStatusCode: HttpStatusCode, logIt: Boolean = true) = exception<T> { cause ->
    APIError(httpStatusCode, cause.message ?: "").let {
        call.respond(it.httpStatusCode, it)
    }
    if (logIt) throw cause
}

