package org.phillipz.kommentor.repo

import org.phillipz.kommentor.model.Comment
import org.phillipz.kommentor.model.CommentInput
import org.phillipz.kommentor.model.CommentStatus
import java.time.ZoneId
import java.time.ZonedDateTime
import kotlin.random.Random


/**
 * Basic, in-memory representation of the [CommentRepo].
 */
class InMemoryCommentRepo(private val data: MutableMap<String, List<Comment>>) : CommentRepo {

    override fun getComment(id: String): Comment? {
        return data.values.flatten().firstOrNull { it.id == id }
    }

    override fun getComments(subjectId: String, statuses: List<CommentStatus>): List<Comment> {
        return data[subjectId]
            ?.filter { statuses.contains(it.status) }
            ?.sortedBy { it.created }
            ?: emptyList()
    }

    override fun createComment(user: String, input: CommentInput): Comment {
        return comment(user, input.subjectId, input.text).also {
            data[input.subjectId] = data.getOrDefault(input.subjectId, emptyList()).plus(it)
        }
    }

    override fun editComment(id: String, newText: String): Comment? {
        return modifyComment(id, newText, CommentStatus.Edited)
    }

    override fun deleteComment(id: String): Comment? {
        return modifyComment(id, "**[REMOVED]**", CommentStatus.Deleted)
    }


    private fun modifyComment(id: String, newText: String, status: CommentStatus): Comment? {
        return getComment(id)?.let {
            check(it.status != CommentStatus.Deleted) { "Can't edit or delete 'Deleted' comments" }
            val editedComment = it.copy(
                text = newText,
                updated = ZonedDateTime.now(ZoneId.of("UTC")),
                status = status
            )
            data[it.subjectId] = data.getOrDefault(it.subjectId, emptyList()).minus(it).plus(editedComment)
            editedComment
        }
    }
}


fun comment(user: String, subjectId: String, text: String) = Comment(
    userName = user,
    subjectId = subjectId,
    text = text,
    created = ZonedDateTime.now(ZoneId.of("UTC")).minusMinutes(Random.nextLong(60))
)