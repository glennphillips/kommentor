package org.phillipz.kommentor.repo


val commentData = mutableMapOf(
    with("368578be-6319-4127-823e-0d4b9813a3c8") {
        this to listOf(
            comment("gphillips", this, "comment 1"),
            comment("jdoe", this, "comment 1"),
            comment("jdoe", this, "comment 1")
        )
    },
    with("fafd182a-939b-4495-b5ce-4f763ec1db2a") {
        this to listOf(
            comment("gphillips", this, "comment 1"),
            comment("jdoe", this, "comment 1"),
            comment("jdoe", this, "comment 1")
        )
    }
)

