package org.phillipz.kommentor.repo

import org.phillipz.kommentor.model.Comment
import org.phillipz.kommentor.model.CommentInput
import org.phillipz.kommentor.model.CommentStatus

interface CommentRepo {
    fun getComment(id: String): Comment?
    fun getComments(subjectId: String, statuses: List<CommentStatus> = activeStatus): List<Comment>
    fun createComment(user: String, input: CommentInput): Comment
    fun editComment(id: String, newText: String): Comment?
    fun deleteComment(id: String): Comment?
}

val activeStatus = listOf(CommentStatus.Created, CommentStatus.Edited)
val allStatus = CommentStatus.values().toList()
