package org.phillipz.kommentor.routes

import io.ktor.application.*
import io.ktor.auth.*
import io.ktor.features.*
import io.ktor.request.*
import io.ktor.response.*
import io.ktor.routing.*
import org.phillipz.kommentor.config.User
import org.phillipz.kommentor.model.CommentInput
import org.phillipz.kommentor.repo.CommentRepo

private fun ApplicationCall.userName(): String = this.principal<User>()?.userName.let {
    requireNotNull(it) { "Unable to obtain userName from credentials" }
}

/**
 * Create a new Comment. Expects a [CommentInput] as JSON.
 *
 * ### Note:
 * * At this time, there are no checks for a user being authorized to a given Subject
 * * Expects valid JWT Bearer Token
 *
 * @return [Comment] as JSON
 */
fun Route.createComment(commentRepo: CommentRepo) {
    authenticate {
        post("/comments") {
            with(call.receive<CommentInput>()) {
                validate()
                call.respond(commentRepo.createComment(call.userName(), this))
            }
        }
    }
}

/**
 * Retrieve all [Comment]s for a given 'subjectId'. If none exist, returns an empty list
 *
 * ### Note:
 * * At this time, there are no checks for a user being authorized to a given Subject
 * * Expects valid JWT Bearer Token
 *
 * @param subjectId [Query Param]
 * @return [List<Comment>] as JSON
 */
// TODO: Add param for comment statuses
fun Route.comments(commentRepo: CommentRepo) {
    authenticate {
        get("/comments") {
            call.request.queryParameters["subjectId"].also {
                requireNotNull(it) { "A 'subjectId' query parameter is required to retrieve comments" }
                call.respond(commentRepo.getComments(it))
            }
        }
    }
}

/**
 * Retrieve [Comment] for a given 'id'. If none exist, returns 404 Not Found
 *
 * ### Note:
 * * At this time, there are no checks for a user being authorized to a given Subject
 * * Expects valid JWT Bearer Token
 *
 * @param id [Path Param]
 * @return [Comment] as JSON
 */
fun Route.commentById(commentRepo: CommentRepo) {
    authenticate {
        get("/comments/{id}") {
            // if 'id' is null or empty, it will not match this route. Safe to assume, if here, the value is legit
            call.parameters["id"]!!.also {
                call.respond(commentRepo.getComment(it) ?: throw NotFoundException("No comment found for id [$it]"))
            }
        }
    }
}

/**
 * Edit the text for an existing [Comment] by the given 'id'. If that comment can't be found, returns 404 Not Found
 *
 * ### Note:
 * * At this time, there are no checks for a user being authorized to a given Subject
 * * Expects valid JWT Bearer Token
 *
 * @param id [Path Param] - the comment id
 * @param text [Query Param] - the modified comment text
 * @return The updated [Comment] as JSON
 */
fun Route.editComment(commentRepo: CommentRepo) {
    authenticate {
        put("/comments/{id}") {
            call.parameters["id"]!!.also {
                call.respond(
                    commentRepo.editComment(it, call.request.queryParameters["text"] ?: "")
                        ?: throw NotFoundException("No comment found for id [$it]")
                )
            }
        }
    }
}

/**
 * Delete an existing [Comment] by the given 'id'. If that comment can't be found, returns 404 Not Found
 * A 'Delete' redacts the text and marks the status as [CommentStatus.Deleted] but leaves the [Comment]
 *
 * ### Note:
 * * At this time, there are no checks for a user being authorized to a given Subject
 * * Expects valid JWT Bearer Token
 *
 * @param id [Path Param]
 * @return The updated [Comment] as JSON
 */
fun Route.deleteComment(commentRepo: CommentRepo) {
    authenticate {
        delete("/comments/{id}") {
            call.parameters["id"]!!.also {
                call.respond(
                    commentRepo.deleteComment(it)
                        ?: throw NotFoundException("No comment found for id [$it]")
                )
            }
        }
    }
}
