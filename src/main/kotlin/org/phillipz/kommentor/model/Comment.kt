package org.phillipz.kommentor.model

import com.fasterxml.jackson.annotation.JsonFormat
import java.time.ZoneId
import java.time.ZonedDateTime
import java.util.*

data class CommentInput(
    val subjectId: String = "",
    val text: String = ""
) {
   fun validate() {
       require(subjectId.isNotBlank()) { "'subjectId' is expected to contain a value" }
       require(text.isNotBlank()) { "'text' is expected to contain a value" }
   }
}


data class Comment(
    val id: String = UUID.randomUUID().toString(),

    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd'T'HH:mm:ss.SSSZ")
    val created: ZonedDateTime = ZonedDateTime.now(ZoneId.of("UTC")),

    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd'T'HH:mm:ss.SSSZ")
    val updated: ZonedDateTime? = null,
    val userName: String,
    val subjectId: String,
    val text: String,
    val status: CommentStatus = CommentStatus.Created
)

enum class CommentStatus { Created, Deleted, Edited }