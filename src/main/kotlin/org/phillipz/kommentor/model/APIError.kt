package org.phillipz.kommentor.model

import com.fasterxml.jackson.annotation.JsonIgnore
import io.ktor.http.*

/**
 * A standard data representation of an API Error Response
 */
data class APIError(@JsonIgnore val httpStatusCode: HttpStatusCode, val message: String) {
    val code = httpStatusCode.value
    val description = httpStatusCode.description
}
