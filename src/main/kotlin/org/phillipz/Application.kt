package org.phillipz

import com.typesafe.config.ConfigFactory
import io.ktor.application.*
import io.ktor.config.*
import io.ktor.routing.*
import io.ktor.server.engine.*
import io.ktor.server.netty.*
import org.phillipz.kommentor.config.configureContentNegotiation
import org.phillipz.kommentor.config.configureExceptionHandling
import org.phillipz.kommentor.config.configureSecurity
import org.phillipz.kommentor.config.intProperty
import org.phillipz.kommentor.config.stringProperty
import org.phillipz.kommentor.repo.InMemoryCommentRepo
import org.phillipz.kommentor.repo.commentData
import org.phillipz.kommentor.routes.commentById
import org.phillipz.kommentor.routes.comments
import org.phillipz.kommentor.routes.createComment
import org.phillipz.kommentor.routes.deleteComment
import org.phillipz.kommentor.routes.editComment

fun main() {
    embeddedServer(Netty, environment = applicationEngineEnvironment {
        config = HoconApplicationConfig(ConfigFactory.load())

        module { configureModules(config) }

        connector {
            port = config.intProperty("ktor.server.port")
            host = config.stringProperty("ktor.server.host")
        }
    }).start(true)
}

fun Application.configureModules(config: ApplicationConfig) {
    // Everything below could be part of a shared library for standard usage across services
    configureSecurity(config)
    configureContentNegotiation(config)
    configureExceptionHandling(config)

    // custom routing unique to this service
    configureRouting(config)
}

fun Application.configureRouting(config: ApplicationConfig) {
    val commentRepo = InMemoryCommentRepo(commentData)
    routing {
        createComment(commentRepo)
        comments(commentRepo)
        commentById(commentRepo)
        editComment(commentRepo)
        deleteComment(commentRepo)
    }
}


